package net.qsoft.monitoringnewway.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import net.qsoft.monitoringnewway.Model.Monitor;
import net.qsoft.monitoringnewway.NetworkService.CloudRequest;
import net.qsoft.monitoringnewway.NetworkService.ErrorDialog;
import net.qsoft.monitoringnewway.NetworkService.ProgressDialog;
import net.qsoft.monitoringnewway.NetworkService.VolleyCustomRequest;
import net.qsoft.monitoringnewway.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.android.volley.VolleyLog.d;

/**
 * Created by QSPA10 on 3/14/2018.
 */

public class ActivityMonitor extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private static final String TAG = ActivityMonitor.class.getSimpleName();

    private ArrayList<Monitor> monitorsArrayList;
    ArrayList<String> monitorSpinnerData = new ArrayList<>();
    private Spinner monitorSpinner;
    ProgressDialog progressDialog;
    ErrorDialog errorDialog;
    private static final String SPINNER_URL = "http://103.43.93.162/mnw/monitor_selection";
    private static final String hit_url = "http://103.43.93.162/mnw/monitor_selection";
    private String selectMonitor, monitor_id, pos;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.monitor_select_layout);
        initializeViews();
        loadServerData();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void loadServerData() {
        progressDialog.showProgress();
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, SPINNER_URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "Server Response: " + response.toString());
                try {
                    monitorsArrayList = new ArrayList<>();


                    String status = response.getString("status");
                    Log.d("Message", status);

                    JSONArray mdata = response.getJSONArray("data");

                    // Getting JSON Array node

                    if (status != null) {
                        // looping through All Contacts
                        for (int i = 0; i < mdata.length(); i++) {

                            JSONObject c = mdata.getJSONObject(i);

                            Monitor monitor = new Monitor();
                            monitor.setBranch_code(c.getString("branchcode"));
                            monitor.setDate_start(c.getString("datestart"));
                            monitor.setDate_end(c.getString("dateend"));
                            monitor.setMonitor1_code(c.getString("monitor1_code"));
                            monitor.setMonitor2_code(c.getString("monitor2_code"));

                            monitorsArrayList.add(monitor);
                        }
                    }

                    if (!monitorsArrayList.isEmpty()) {
                        setMonitorSpinnerData(monitorsArrayList);
                    }

                } catch (JSONException ex) {
                    Toast.makeText(ActivityMonitor.this, ex.toString(), Toast.LENGTH_SHORT).show();
                    progressDialog.hideProgress();
                    errorDialog.showDialog("Error!", "Try Again Later");
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.hideProgress();
                d(TAG, "Error: " + volleyError.getMessage());

                if (volleyError instanceof NetworkError) {
                    errorDialog.showDialog("No Internet!", "Enable Moblie Data or WIFI");
                } else if (volleyError instanceof ServerError) {
                    errorDialog.showDialog("Server Error!", "Server Not Found,Try Again Later!");

                } else if (volleyError instanceof AuthFailureError) {
                    errorDialog.showDialog("No Internet!", "Enable Moblie Data or WIFI");

                } else if (volleyError instanceof ParseError) {

                    errorDialog.showDialog("Parsing Error!", "Parsing Error, Try Again Later.");
                } else if (volleyError instanceof NoConnectionError) {
                    errorDialog.showDialog("No Internet!", "Enable Moblie Data or WIFI");
                } else if (volleyError instanceof TimeoutError) {
                    errorDialog.showDialog("Request Timeout!", "Please Check Your Internet Connection");
                }
            }
        });

        CloudRequest.getInstance(this).addToRequestQueue(jsonObjectRequest);
        progressDialog.hideProgress();

        CloudRequest.getInstance(this).addToRequestQueue(jsonObjectRequest);
        progressDialog.hideProgress();
    }

    public void dataSendToServer() {

        progressDialog.showProgress();

        HashMap<String, String> params = new HashMap<>();
        params.put("position", pos); //Items - Item 1 - name
        params.put("monitor_id", monitor_id);

        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hit_url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            Log.d("Message", status);
                            if (status == "sucess") {
                                progressDialog.hideProgress();
                                Toast.makeText(getApplicationContext(),
                                        response.getString("message"), Toast.LENGTH_SHORT).show();
                            } else {
                                progressDialog.hideProgress();
                                errorDialog.showDialog("Error!", "Try Again Later");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.hideProgress();
                            errorDialog.showDialog("Error!", "Try Again Later.");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.hideProgress();
                        d(TAG, "Error: " + volleyError.getMessage());

                        if (volleyError instanceof NetworkError) {
                            errorDialog.showDialog("No Internet!", "Enable Moblie Data or WIFI");
                        } else if (volleyError instanceof ServerError) {
                            errorDialog.showDialog("Server Error!", "Server Not Found,Try Again Later!");

                        } else if (volleyError instanceof AuthFailureError) {
                            errorDialog.showDialog("No Internet!", "Enable Moblie Data or WIFI");

                        } else if (volleyError instanceof ParseError) {

                            errorDialog.showDialog("Parsing Error!", "Parsing Error, Try Again Later.");
                        } else if (volleyError instanceof NoConnectionError) {
                            errorDialog.showDialog("No Internet!", "Enable Moblie Data or WIFI");
                        } else if (volleyError instanceof TimeoutError) {
                            errorDialog.showDialog("Request Timeout!", "Please Check Your Internet Connection");
                        }


                    }
                });

        CloudRequest.getInstance(this).addToRequestQueue(postRequest);
        progressDialog.hideProgress();
    }

    private void setMonitorSpinnerData(ArrayList<Monitor> monitorsArrayList) {
        ArrayList<String> monitor = new ArrayList<>();
        monitor.add(getResources().getString(R.string.monitor_select));
        for (int i = 0; i < monitorsArrayList.size(); i++) {
            //monitor.add(monitorsArrayList.get(i).getBranch_code());
            //monitor.add(monitorsArrayList.get(i).getDate_start());
            //monitor.add(monitorsArrayList.get(i).getDate_end());
            monitor.add(monitorsArrayList.get(i).getMonitor1_code());
            monitor.add(monitorsArrayList.get(i).getMonitor2_code());
        }
        ArrayAdapter<String> monitorAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, monitor);
        monitorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        monitorSpinner.setAdapter(monitorAdapter);
    }

    private void initializeViews() {
        submit = (Button) findViewById(R.id.btn_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataSendToServer();
            }
        });
        monitorSpinner = (Spinner) findViewById(R.id.spinner_monitor);
        monitorSpinner.setOnItemSelectedListener(this);
        progressDialog = new ProgressDialog(ActivityMonitor.this);
        errorDialog = new ErrorDialog(ActivityMonitor.this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Spinner spinner = (Spinner) parent;
        if (spinner.getId() == R.id.spinner_monitor) {
            selectMonitor = monitorSpinner.getSelectedItem().toString();
            if (selectMonitor.equals(getResources().getString(R.string.monitor_select))) {
                monitor_id = "";
            } else {
                pos = String.valueOf(position);
                monitor_id = parent.getItemAtPosition(position).toString();
                Toast.makeText(parent.getContext(), "Monitor:" + pos + "&" + "Monitor id:" + monitor_id, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
