package net.qsoft.monitoringnewway.Model;

/**
 * Created by QSPA10 on 3/4/2018.
 */

public class Monitor {
    private String monitor_id;
    private String monitor_no;

    private String branch_code;
    private String date_start;
    private String date_end;
    private String monitor1_code;
    private String monitor2_code;

    public String getBranch_code() {
        return branch_code;
    }

    public void setBranch_code(String branch_code) {
        this.branch_code = branch_code;
    }

    public String getDate_start() {
        return date_start;
    }

    public void setDate_start(String date_start) {
        this.date_start = date_start;
    }

    public String getDate_end() {
        return date_end;
    }

    public void setDate_end(String date_end) {
        this.date_end = date_end;
    }

    public String getMonitor1_code() {
        return monitor1_code;
    }

    public void setMonitor1_code(String monitor1_code) {
        this.monitor1_code = monitor1_code;
    }

    public String getMonitor2_code() {
        return monitor2_code;
    }

    public void setMonitor2_code(String monitor2_code) {
        this.monitor2_code = monitor2_code;
    }

    public String getMonitor_id() {
        return monitor_id;
    }

    public void setMonitor_id(String monitor_id) {
        this.monitor_id = monitor_id;
    }

    public String getMonitor_no() {
        return monitor_no;
    }

    public void setMonitor_no(String monitor_no) {
        this.monitor_no = monitor_no;
    }

}
